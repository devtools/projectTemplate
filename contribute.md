# Contribute

Repository: https://github.com/dral/project

## Project structure

### Concepts

### Walkthrough

## Development environment

### Versionning

This project uses the following principles:

- [git flow](https://github.com/nvie/gitflow) where the development branch is called `dev`.
- [semantic versionning](http://semver.org/).
- [formatted commit messages](https://gitlab.com/devtools/gittemplate).

### Debug

### Build

### Test

### Documentation

### Publish
