# template

Template project.

## Introduction

Look how easy it is to use:

```
import project
# Get your stuff done
project.do_stuff()
```

### Features

- [x] Implemented feature
- [x] ~~Deprecated feature~~
- [ ] Missing feature


## Documentation

### Install

```
install project
```

### Use case 1

### Use case 2

## Support

If you are having issues, please report them in the [github repository](1).

## Contribute

Explore/fork the [github](1) repository or see the  [contribute guide](./contribute.md) for more details on the project's internals.

## License

The project is licensed under the [MIT license](http://opensource.org/licenses/MIT).

[1](https://github.com/dral/project "Project repository")
